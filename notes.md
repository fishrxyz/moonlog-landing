## Keywords from competitors

helps collect and organise feature requests
prioritize your roadmap (don't waste time and human resources building the wrong thing, engage users and team members)
make sure your customers are up to date

capture user feedback
extract insights and inform product direction
anylyse feedback
create a roadmap (why publish a roadmap ?)
communiate your releases (why a changelog ?)

integrations with github slack jira and many more to come in the future

make your users happy
build the product your users want with customer feedback
find out what you should be working on
Turn User Feedback Into Actionable Product Decisions
We help product managers turbocharge their development process by making it easy to centralize feedback, prioritize ideas and delight users by shipping exactly what they're looking for.
keep your users and shareholders in the know
understand your customers pain points and close the feedback loop
Moonlog helps you build the product your customers want
Know what to build next with customer info
Impress potential customers with your progress and build customer trust. reassure potential customers
Unlock invaluable customer insights and build a better product, faster.

Craft beautiful, engaging release notes to keep customers and stakeholders up to date, or as a marketing tool to convert more.
Create product updates in minutes, with cover images and a beautiful writing experience

Publishing and promoting release notes increases awareness, engagement, and trust.

# use cases

feature request management
public roadmap
idea managements

# competitions

https://feedback.fish/
https://roadmap.space/
https://www.cycle.app/
https://frill.co

# sections

- Build a changelog
- User feedback

# features

Moderation
Minimal design
Feature voting
Fully customizable (add your own domain and branding)
Bug reports
Integrations (slack, github, jira, trello, gsuite etc ) [soon]
Product roadmap (soon)
Invite your team (soon)
Import from canny (soon)
Export your data
