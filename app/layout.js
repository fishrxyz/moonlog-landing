import "./globals.css";
import { DM_Sans } from "next/font/google";

const inter = DM_Sans({
  weight: ["400", "500", "700"],
  subsets: ["latin"],
});

export const metadata = {
  title: "moonlog.co - Release management software for teams",
  description:
    "moonlog helps you collect and analyze user feedback to better understand their needs and prioritize your product's roadmap",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
  );
}
