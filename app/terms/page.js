import MainNav from "../components/MainNav";
import MoonlogFooter from "../components/home/MoonlogFooter";

export const metadata = {
  title:
    "Terms of service | moonlog.co - Release management software for teams",
  description:
    "moonlog helps you collect and analyze user feedback to better understand their needs and prioritize your product's roadmap",
};

export default function Terms() {
  return (
    <main className="min-h-screen">
      <div className="py-10 px-24">
        <MainNav />
      </div>
      <div className="max-w-[90%] md:max-w-5xl mx-auto mb-20">
        <div class="mt-20">
          <h2 className="title font-bold text-5xl text-center mb-20 md:mb-[11rem] md:text-7xl">
            Moonlog's terms of service
          </h2>

          <h2 className="font-bold md:text-4xl text-2xl mb-10">
            1. User's acknowledgements
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            By using any of Moonlog services (“Services”) you agree to be bound
            by the following Terms of Service (“Terms), (further referred as
            “Moonlog”, “we”, “us”, “our”) reserves the right to change or amend
            these Terms at any time without notice. We encourage you to review
            these Terms regularly, as your continued use of the Services will
            confirm your acceptance of the revised Terms. If you do not want to
            be bound to these Terms, please do not sign up for the Services.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            BY USING THIS SITE, YOU AGREE TO BE BOUND BY THESE TERMS OF USE. IF
            YOU DO NOT WISH TO BE BOUND BY THE THESE TERMS OF USE, PLEASE EXIT
            THE SITE NOW. YOUR REMEDY FOR DISSATISFACTION WITH THIS SITE, OR ANY
            PRODUCTS, SERVICES, CONTENT, OR OTHER INFORMATION AVAILABLE ON OR
            THROUGH THIS SITE, IS TO STOP USING THE SITE AND/OR THOSE PARTICULAR
            PRODUCTS OR SERVICES. YOUR AGREEMENT WITH US REGARDING COMPLIANCE
            WITH THESE TERMS OF USE BECOMES EFFECTIVE IMMEDIATELY UPON
            COMMENCEMENT OF YOUR USE OF THIS SITE.
          </p>

          <h2 className="font-bold md:text-4xl text-2xl mb-10">
            2. Membership
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Registration. To fully use our service you must register as a member
            by providing complete and accurate full name, password and email
            address. You are not allowed to use someone else’s name or a name
            that violates any third party rights.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Emails. Primary means of communication between you and us will be
            through emails. Thus, you acknowledge that we may use your email to
            notify you about upcoming system updates, new product releases,
            company news, changes of your account status etc., through emails.
            We encourage you to carefully read our emails to stay informed. We
            will not carry any responsibility if some information did not reach
            you as a result of failing to read our emails.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Account security. You must safeguard confidentiality of your job
            site’s access credentials. You are also responsible for all
            activities that occur under your account. Moonlog will not be held
            responsibility for an unauthorized access to your account. If you
            become aware of any unauthorized access to your account, you must
            change your password and notify us as soon as possible.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10">
            3. Account Ownership
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            If you are signing up on behalf of your employer then account owner
            is your employer. You warrant that you have authority to bind your
            employer to these Terms.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            You can transfer your account and pass account ownership rights to a
            third party at any time. In this case you need to inform us and
            update account information accordingly. Once your account is
            transferred to another owner you cannot claim ownership rights for
            this account. This paragraph does not apply to lifetime licenses.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Moonlog reserves the right, to the extent of its knowledge and
            ability, to determine the rightful account owner in case of
            disputes. If we are not able to determine the rightful owner, we can
            terminate an account until the resolution between conflicting
            parties is found. If there are some amounts due, the person or
            persons determined to be the rightful owners will be obliged to pay
            them.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10">4. Fees payment</h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Payments. The system will charge you automatically every billing
            cycle. All Moonlog services are prepaid and will not begin until
            full payment is received. No credits for unperformed services are
            possible. Moonlog does not issue either partial or full refunds,
            including the cases when your services are suspended or terminated
            before the end of the Services.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Modification to the pricing and terms. Moonlog retains the right to
            modify subscription plans’ pricing and terms at any time without
            additional notice. All changes will be posted in the corresponding
            sections of the website.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10">
            5. Billing Disputes
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            It is your responsibility to review all monthly charges for
            accuracy. Within 7 days you can dispute the charge. After 7 days we
            will consider that all charges are valid and thereby, you waive any
            claims regarding this charge.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10">
            6. Termination and Cancellation of Service
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            You can terminate Service at any time. To make sure your account is
            not billed for another month of service, you must initiate service
            cancellation before your billing cycle due date.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            7. System Updates
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            We reserve the right to update the system. Updates may include, but
            not limited to, adding/ removing functionalities, bugs fixes etc.
            There are no warranties that updated version will not contain bugs
            or will not affect your use of the Services. We can initiate update
            at any time without additional notice. Moonlog will not carry any
            responsibility for any modifications in code not provided by Moonlog
            team. Restoration of modified code is your responsibility and any
            associated costs are to be met by you.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            8. Use of Customer’s Name and Trademark
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Moonlog owns the platform, but we will not claim any rights for your
            trade names, trademarks, or data uploaded into our stores. With your
            written consent we may use your trade name, trademark, full name
            and/or your web-site URL in our live example section for advertising
            purposes.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Customer may use Moonlog’s trade name, trademarks, and service marks
            (collectively, “Moonlog’s Marks) for advertising purposes. Customer
            must submit a copy of advertising materials to Moonlog for its prior
            written consent.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            8. Other Conditions
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            We reserve the right to terminate the Service at any time
          </p>
          <p className="md:text-xl text-lg leading-normal mb-6">
            Any kind of verbal or written insult of Moonlog member/employee will
            result in immediate account termination.
          </p>
          <p className="md:text-xl text-lg leading-normal mb-6">
            At our sole discretion we reserve the right to remove any content if
            we determine, in our sole discretion, it to be unlawful, offensive,
            threatening or violating third party copyrights
          </p>
          <p className="md:text-xl text-lg leading-normal mb-6">
            All sample data, including but not limited to, sample images, job
            and resume descriptions, sample emails texts etc, used in the
            Service are to demonstrate Service in operation. It is your
            responsibility to change it in accordance with your legal and
            business needs
          </p>
          <p className="md:text-xl text-lg leading-normal mb-6">
            We do not bear any responsibility for the content uploaded by our
            clients. All claims are to be addressed directly to the clients
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            9. Prohibited Activities
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            In addition to other restrictions outlined in these Terms, you agree
            that you will not:
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Use the Services for any illegal purposes, beyond the scope of their
            intended use, or otherwise prohibited in these Terms.
          </p>
          <p className="md:text-xl text-lg leading-normal mb-6">
            Publish Content which is unlawful, offensive, threatening, libelous,
            defamatory, pornographic, obscene or otherwise objectionable or
            violates any party’s intellectual property or these Terms of
            Service.
          </p>
          <p className="md:text-xl text-lg leading-normal mb-6">
            Sell, donate or otherwise transfer lifetime licenses.
          </p>
          <p>Compromise security of the Service</p>
          <p className="md:text-xl text-lg leading-normal mb-6">
            Send any unsolicited advertising, spam or any other unauthorized
            promotional materials
          </p>
          <p className="md:text-xl text-lg leading-normal mb-6">
            Attempt to copy or replicate the Service provided in any shape or
            form.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            10. Disclaimer of Warranties
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            ALL MATERIALS AND SERVICES ON THIS SITE ARE PROVIDED ON AN "AS IS"
            AND "AS AVAILABLE" BASIS WITHOUT WARRANTY OF ANY KIND, EITHER
            EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
            WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE,
            OR THE WARRANTY OF NON-INFRINGEMENT. WITHOUT LIMITING THE FOREGOING,
            WE MAKE NO WARRANTY THAT (A) THE SERVICES AND MATERIALS WILL MEET
            YOUR REQUIREMENTS, (B) THE SERVICES AND MATERIALS WILL BE
            UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE, (C) THE RESULTS THAT
            MAY BE OBTAINED FROM THE USE OF THE SERVICES OR MATERIALS WILL BE
            EFFECTIVE, ACCURATE OR RELIABLE, OR (D) THE QUALITY OF ANY PRODUCTS,
            SERVICES, OR INFORMATION PURCHASED OR OBTAINED BY YOU FROM THE SITE
            FROM US OR OUR AFFILIATES WILL MEET YOUR EXPECTATIONS OR BE FREE
            FROM MISTAKES, ERRORS OR DEFECTS.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            THIS SITE COULD INCLUDE TECHNICAL OR OTHER MISTAKES, INACCURACIES OR
            TYPOGRAPHICAL ERRORS. WE MAY MAKE CHANGES TO THE MATERIALS AND
            SERVICES AT THIS SITE, INCLUDING THE PRICES AND DESCRIPTIONS OF ANY
            PRODUCTS LISTED HEREIN, AT ANY TIME WITHOUT NOTICE. THE MATERIALS OR
            SERVICES AT THIS SITE MAY BE OUT OF DATE, AND WE MAKE NO COMMITMENT
            TO UPDATE SUCH MATERIALS OR SERVICES.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            THE USE OF THE SERVICES OR THE DOWNLOADING OR OTHER ACQUISITION OF
            ANY MATERIALS THROUGH THIS SITE IS DONE AT YOUR OWN DISCRETION AND
            RISK AND WITH YOUR AGREEMENT THAT YOU WILL BE SOLELY RESPONSIBLE FOR
            ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM
            SUCH ACTIVITIES.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Through your use of the site, you may have the opportunities to
            engage in commercial transactions with other users and vendors. You
            acknowledge that all transactions relating to any merchandise or
            services offered by any party, including, but not limited to the
            purchase terms, payment terms, warranties, guarantees, maintenance
            and delivery terms relating to such transactions, are agreed to
            solely between the seller or purchaser of such merchandize and
            services and you. WE MAKE NO WARRANTY REGARDING ANY TRANSACTIONS
            EXECUTED THROUGH, OR IN CONNECTION WITH THIS SITE, AND YOU
            UNDERSTAND AND AGREE THAT SUCH TRANSACTIONS ARE CONDUCTED ENTIRELY
            AT YOUR OWN RISK. ANY WARRANTY THAT IS PROVIDED IN CONNECTION WITH
            ANY PRODUCTS, SERVICES, MATERIALS, OR INFORMATION AVAILABLE ON OR
            THROUGH THIS SITE FROM A THIRD PARTY IS PROVIDED SOLELY BY SUCH
            THIRD PARTY, AND NOT BY US OR ANY OTHER OF OUR AFFILIATES.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Content available through this site often represents the opinions
            and judgments of an information provider, site user, or other person
            or entity not connected with us. We do not endorse, nor are we
            responsible for the accuracy or reliability of, any opinion, advice,
            or statement made by anyone other than an authorized stack.app
            spokesperson speaking in his/her official capacity. Please refer to
            the specific editorial policies posted on various sections of this
            site for further information, which policies are incorporated by
            reference into these Terms of Use.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            You understand and agree that temporary interruptions of the
            services available through this site may occur as normal events. You
            further understand and agree that we have no control over third
            party networks you may access in the course of the use of this site,
            and therefore, delays and disruption of other network transmissions
            are completely beyond our control.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            You understand and agree that the services available on this site
            are provided "AS IS" and that we assume no responsibility for the
            timeliness, deletion, mis-delivery or failure to store any user
            communications or personalization settings.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN
            WARRANTIES, SO SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            11. Limitation of Liability
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN
            IN NO EVENT SHALL WE OR OUR AFFILIATES BE LIABLE TO YOU OR ANY THIRD
            PARTY FOR ANY SPECIAL, PUNITIVE, INCIDENTAL, INDIRECT OR
            CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER,
            INCLUDING, WITHOUT LIMITATION, THOSE RESULTING FROM LOSS OF USE,
            DATA OR PROFITS, WHETHER OR NOT WE HAVE BEEN ADVISED OF THE
            POSSIBILITY OF SUCH DAMAGES, AND ON ANY THEORY OF LIABILITY, ARISING
            OUT OF OR IN CONNECTION WITH THE USE OF THIS SITE OR OF ANY WEB SITE
            REFERENCED OR LINKED TO FROM THIS SITE.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            FURTHER, WE SHALL NOT BE LIABLE IN ANY WAY FOR THIRD PARTY GOODS AND
            SERVICES OFFERED THROUGH THIS SITE OR FOR ASSISTANCE IN CONDUCTING
            COMMERCIAL TRANSACTIONS THROUGH THIS SITE, INCLUDING WITHOUT
            LIMITATION THE PROCESSING OF ORDERS.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            SOME JURISDICTIONS PROHIBIT THE EXCLUSION OR LIMITATION OF LIABILITY
            FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SO THE ABOVE LIMITATIONS
            MAY NOT APPLY TO YOU.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            12. Indemnification
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Upon a request by us, you agree to defend, indemnify, and hold us
            and our Affiliates harmless from all liabilities, claims, and
            expenses, including attorney’s fees, that arise from your use or
            misuse of this site. We reserve the right, at our own expense, to
            assume the exclusive defense and control of any matter otherwise
            subject to indemnification by you, in which event you will cooperate
            with us in asserting any available defenses.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            13. Security and Password
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            You are solely responsible for maintaining the confidentiality of
            your password and account and for any and all statements made and
            acts or omissions that occur through the use of your password and
            account. Therefore, you must take steps to ensure that others do not
            gain access to your password and account. Our personnel will never
            ask you for your password. You may not transfer or share your
            account with anyone, and we reserve the right to immediately
            terminate your account if you do transfer or share your account.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            14. Participation in Promotions
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            From time to time, this site may include advertisements offered by
            third parties. You may enter into correspondence with or participate
            in promotions of the advertisers showing their products on this
            site. Any such correspondence or promotions, including the delivery
            of and the payment for goods and services, and any other terms,
            conditions, warranties or representations associated with such
            correspondence or promotions, are solely between you and the
            advertiser. We assume no liability, obligation or responsibility for
            any part of any such correspondence or promotion.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            15. International Use
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Although this site may be accessible worldwide, we make no
            representation that materials on this site are appropriate or
            available for use in locations outside the United States, and
            accessing them from territories where their contents are illegal is
            prohibited. Those who choose to access this site from other
            locations do so on their own initiative and are responsible for
            compliance with local laws. Any offer for any product, service,
            and/or information made in connection with this site is void where
            prohibited.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            16. Termination of Use
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            You agree that we may, in our sole discretion, terminate or suspend
            your access to all or part of the site with or without notice and
            for any reason, including, without limitation, breach of these Terms
            of Use. Any suspected fraudulent, abusive or illegal activity may be
            grounds for terminating your relationship and may be referred to
            appropriate law enforcement authorities.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Upon termination or suspension, regardless of the reasons therefore,
            your right to use the services available on this site immediately
            ceases, and you acknowledge and agree that we may immediately
            deactivate or delete your account and all related information and
            files in your account and/or bar any further access to such files or
            this site. We shall not be liable to you or any third party for any
            claims or damages arising out of any termination or suspension or
            any other actions taken by us in connection with such termination or
            suspension.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            17. Governing Law
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            This site (excluding any linked sites) is controlled by us from our
            offices within the Delaware, United States of America. It can be
            accessed from all 50 states, as well as from other countries around
            the world. As each of these places has laws that may differ from
            those of Delaware, by accessing this site both of us agree that the
            statutes and laws of the State of Delaware, without regard to the
            conflicts of laws principles thereof and the United Nations
            Convention on the International Sales of Goods, will apply to all
            matters relating to the use of this site and the purchase of
            products and services available through this site. Each of us agrees
            and hereby submits to the exclusive personal jurisdiction and venue
            any court of competent jurisdiction within the State of Delaware
            with respect to such matters.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            18. Notices
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            All notices to a party shall be in writing and shall be made either
            via email or conventional mail. Notices to us must be sent to the
            attention of Customer Service at support@moonlog.co. if by
            conventional mail. Notices to you may be sent to the address
            supplied by you as part of your Registration Data. In addition, we
            may broadcast notices or messages through the site to inform you of
            changes to the site or other matters of importance, and such
            broadcasts shall constitute notice to you at the time of sending.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            19. Entire Agreement
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            These terms and conditions constitute the entire agreement and
            understanding between us concerning the subject matter of this
            agreement and supersedes all prior agreements and understandings of
            the parties with respect to that subject matter. These Terms of Use
            may not be altered, supplemented, or amended by the use of any other
            document(s). Any attempt to alter, supplement or amend this document
            or to enter an order for products or services which are subject to
            additional or altered terms and conditions shall be null and void,
            unless otherwise agreed to in a written agreement signed by you and
            us. To the extent that anything in or associated with this site is
            in conflict or inconsistent with these Terms of Use, these Terms of
            Use shall take precedence.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            20. Miscellaneous
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            In any action to enforce these Terms of Use, the prevailing party
            will be entitled to costs and attorneys’ fees. Any cause of action
            brought by you against us or our Affiliates must be instituted with
            one year after the cause of action arises or be deemed forever
            waived and barred.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            You may not assign your rights and obligations under these Terms of
            Use to any party, and any purported attempt to do so will be null
            and void. We may free assign our rights and obligations under these
            Terms of Use.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            In addition to any excuse provided by applicable law, we shall be
            excused from liability for non-delivery or delay in delivery of
            products and services available through our site arising from any
            event beyond our reasonable control, whether or not foreseeable by
            either party, including but not limited to, labor disturbance, war,
            fire, accident, adverse weather, inability to secure transportation,
            governmental act or regulation, and other causes or events beyond
            our reasonable control, whether or not similar to those which are
            enumerated above.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            If any part of these Terms of Use is held invalid or unenforceable,
            that portion shall be construed in a manner consistent with
            applicable law to reflect, as nearly as possible, the original
            intentions of the parties, and the remaining portions shall remain
            in full force and effect.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Any failure by us to enforce or exercise any provision of these
            Terms of Use or related rights shall not constitute a waiver of that
            right or provision.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-10">
            21. Contact Information
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            please contact us at support@moonlog.co.
          </p>
        </div>
        <div className="mt-[6rem]">
          <MoonlogFooter />
        </div>
      </div>
    </main>
  );
}
