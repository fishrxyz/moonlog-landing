import MainNav from "./components/MainNav";
import Changelog from "./components/home/Changelog";
import FeatureList from "./components/home/FeatureList";
import Feedback from "./components/home/Feedback";
import Hero from "./components/home/Hero";
import MassiveEmail from "./components/home/MassiveEmail";
import MoonlogFooter from "./components/home/MoonlogFooter";
import Pricing from "./components/home/Pricing";

export default function Home() {
  return (
    <main className="min-h-screen">
      <div className="py-10 px-24">
        <MainNav />
      </div>
      <div className="max-w-[90%] md:max-w-5xl mx-auto">
        <Hero />
        <Feedback />
        <Changelog />
        <FeatureList />
        <Pricing />
        <MassiveEmail />
        <MoonlogFooter />
      </div>
    </main>
  );
}
