import MainNav from "../components/MainNav";
import MoonlogFooter from "../components/home/MoonlogFooter";

export const metadata = {
  title: "Privacy Policy | moonlog.co - Release management software for teams",
  description:
    "moonlog helps you collect and analyze user feedback to better understand their needs and prioritize your product's roadmap",
};

export default function Privacy() {
  return (
    <main className="min-h-screen">
      <div className="py-10 px-24">
        <MainNav />
      </div>
      <div className="max-w-[90%] md:max-w-5xl mx-auto mb-20">
        <div class="mt-20">
          <h2 className="title font-bold text-5xl text-center mb-20 md:mb-[11rem] md:text-7xl">
            Privacy Policy
          </h2>

          <h2 className="font-bold md:text-4xl text-2xl mb-10">
            Our commitment to privacy
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Your privacy is important to us. To better protect your privacy we
            provide this policy explaining our online information practices and
            the choices you can make about the way your information is collected
            and used. To make this notice easy to find, we make it available on
            our homepage and at every point where personally identifiable
            information may be requested.
          </p>

          <h2 className="font-bold md:text-4xl text-2xl mb-10 mt-20">
            The Information We Collect
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            This policy applies to all information collected or submitted on the
            Moonlog website. The types of personal information collected on our
            pages are: Name, Email address
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-20">
            How We Use Your Information
          </h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            We use the information you provide about yourself when placing an
            order only to complete that order. We do not share this information
            with outside parties except to the extent necessary to complete that
            order.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            We use return email addresses to answer the email we receive. Such
            addresses are not used for any other purpose and are not shared with
            outside parties.
          </p>

          <p className="md:text-xl text-lg leading-normal mb-6">
            Finally, we never use or share the personally identifiable
            information provided to us online in ways unrelated to the ones
            described above without also providing you an opportunity to opt-out
            or otherwise prohibit such unrelated uses.
          </p>

          <h2 class="font-bold md:text-4xl text-2xl mb-10 mt-20">Contact Us</h2>

          <p className="md:text-xl text-lg leading-normal mb-6">
            If you have any questions or comments about this Privacy Policy or
            your personal information, to make an access or correction request,
            to exercise any applicable rights, to make a complaint, or to obtain
            information about our policies and practices, our Privacy Officer
            (or Data Protection Officer) can be reached by mail or email using
            the following contact information: by email at support@moonlog.co.
          </p>
        </div>
        <div className="mt-[6rem]">
          <MoonlogFooter />
        </div>
      </div>
    </main>
  );
}
