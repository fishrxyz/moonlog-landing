import { MoonSat } from "iconoir-react";
import Link from "next/link";

const Logo = ({ beta }) => {
  return (
    <div className="moonlog__logo flex items-center">
      <Link className="flex items-center" href="/">
        <MoonSat width={30} height={30} strokeWidth={2.3} />
        <p className="text-2xl font-semibold ml-1">moonlog</p>
      </Link>
      {beta && (
        <span className="ml-2 text-sm font-medium px-2 py-1 bg-blue-100 rounded-lg">
          beta
        </span>
      )}
    </div>
  );
};

export default Logo;
