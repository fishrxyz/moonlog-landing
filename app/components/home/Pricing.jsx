import { CheckCircle2 } from "lucide-react";

const Pricing = () => {
  return (
    <section className="mb-20 md:mb-[10em]">
      <header className="text-center max-w-4xl mx-auto mb-14 md:mb-20">
        <h2 className="font-bold text-3xl mb-4 md:text-5xl">
          <span className="block mb-4 md:inline md:mb-0">
            Maximum features.
          </span>{" "}
          <span>Minimal Pricing.</span>
        </h2>
        <p
          style={{
            opacity: 0.3,
          }}
          className="font-bold text-xl leading-normal md:text-3xl"
        >
          One single plan. Get access to everything. Forever.
        </p>
      </header>
      <section className="flex flex-col items-center justify-center md:flex-row md:justify-between md:items-start max-w-xl mx-auto">
        <div className="mb-20 md:mb-0">
          <h3 className="text-3xl font-bold mb-8 md:mb-4 md:text-xl">
            What's inside ?
          </h3>
          <ul>
            <li className="flex items-center mb-2">
              <CheckCircle2 size={22} className="mr-2" />
              <span>API access</span>
            </li>
            <li className="flex mb-2">
              <CheckCircle2 size={22} className="mr-2" />
              <span>Custom integrations</span>
            </li>
            <li className="flex mb-2">
              <CheckCircle2 size={22} className="mr-2" />
              <span>Priority support</span>
            </li>
            <li className="flex mb-2">
              <CheckCircle2 size={22} className="mr-2" />
              <span>Single Sign On (SSO)</span>
            </li>
            <li className="flex mb-2">
              <CheckCircle2 size={22} className="mr-2" />
              <span>Constant improvements</span>
            </li>
          </ul>
        </div>
        <div>
          <h3 className="text-2xl font-bold mb-8 md:mb-4 md:text-xl">
            ✨ Special launch price ✨
          </h3>
          <div className="text-center mb-6">
            <p className="font-bold text-5xl">
              <span className="mr-4 text-slate-300 line-through">$59</span>$39
            </p>
            <span className="block mt-4">billed yearly</span>
          </div>
        </div>
      </section>
    </section>
  );
};

export default Pricing;
