const Feedback = () => {
  return (
    <section className="mb-[5em] md:px-0 md:mb-[10em]">
      <header className="mb-10 md:mb-14">
        <h2 className="font-bold text-3xl leading-normal mb-4 md:text-5xl">
          Don't waste time on the wrong features
        </h2>
        <p
          style={{
            opacity: 0.3,
          }}
          className="font-bold text-xl leading-normal md:text-3xl"
        >
          Capture valuable feedback from your users through feature requests and
          bug reports. Use this data to prioritize new features and better
          understand what you should be working on.
        </p>
      </header>
      <figure>
        <img
          alt="collect user feedback screen"
          className="rounded-md shadow-xl"
          src="/feedback.jpeg"
        />
      </figure>
    </section>
  );
};

export default Feedback;
