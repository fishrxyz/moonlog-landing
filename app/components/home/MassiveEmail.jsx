import { Mail } from "lucide-react";

const MassiveEmail = () => {
  return (
    <section className="mb-[5em] md:mb-[10em] text-center">
      <a
        className="font-bold text-4xl transition-all underline md:text-7xl"
        href="mailto:karim.cheurfi+moonlog@gmail.com"
      >
        <span>hello@moonlog.co</span>
      </a>
    </section>
  );
};

export default MassiveEmail;
