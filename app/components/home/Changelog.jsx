const Changelog = () => {
  return (
    <section className="mb-[5em] md:px-0 md:mb-[10em]">
      <header className="mb-10 md:mb-14">
        <h2 className="font-bold text-3xl leading-normal mb-4 md:text-5xl md:mb-6">
          Close the feedback loop with your users
        </h2>
        <p
          style={{
            opacity: 0.3,
          }}
          className="font-bold text-xl leading-normal md:text-3xl"
        >
          Create stunning, interesting release notes as a marketing tool to
          increase conversions or to keep stakeholders and users informed.
          Publishing releases notes usually results in more engagement and
          trust.
        </p>
      </header>
      <figure>
        <img
          className="rounded-md shadow-xl"
          src="/changelog.jpeg"
          alt="collect user feedback"
        />
      </figure>
    </section>
  );
};

export default Changelog;
