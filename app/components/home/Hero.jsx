const Hero = () => {
  return (
    <section
      id="home-hero"
      className="mt-20 mb-[5em] md:px-0 md:mt-36 md:mb-[10em]"
    >
      <div className="mb-10 md:mb-20">
        <h1 className="font-bold text-[45px] leading-normal mb-4 md:text-6xl">
          Make your users happy.
        </h1>
        <p
          style={{
            opacity: 0.3,
          }}
          className="font-bold text-xl md:max-w-[80%] leading-normal md:text-3xl"
        >
          Moonlog helps you collect and analayse user feedback and build the
          product they want.
        </p>
      </div>
      <figure>
        <img
          src="/hero3.png"
          alt="collect user feedback dashboard"
          className="rounded-md shadow-xl"
        />
      </figure>
    </section>
  );
};

export default Hero;
