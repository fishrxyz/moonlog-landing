import Link from "next/link";

const MoonlogFooter = () => {
  return (
    <footer className="mt-10 mb-10 md:mt-[4em] md:mb-[4em] md:px-0">
      <div className="flex flex-col justify-between md:flex-row">
        <div className="basis-1/3 mb-6">
          <h4 className="font-medium mb-4">Moonlog</h4>
          <p className="text-slate-500 leading-normal">
            We are a software team part of the #buildinpublic movement. We're
            100% bootstraped and VC Free.
          </p>
        </div>
        <div className="mb-6">
          <h4 className="font-medium mb-4">Company</h4>
          <ul>
            <li className="text-slate-500 mb-2">
              <Link href="/privacy">Privacy</Link>
            </li>
            <li className="text-slate-500">
              <Link href="/terms">Terms</Link>
            </li>
          </ul>
        </div>
        <div className="">
          <h4 className="font-medium mb-4">Let's connect</h4>
          <ul>
            <li className="text-slate-500 mb-2">
              <a href="mailto:karim.cheurfi+moonlog@gmail.com">E-mail</a>
            </li>
            <li className="text-slate-500">
              <a href="https://twitter.com/MoonlogHQ">Twitter / X</a>
            </li>
          </ul>
        </div>
      </div>
      <hr className="mt-8" />
      <div className="flex items-center justify-between mt-6">
        <p className="text-slate-400 text-xs">2023 - Moonlog</p>
      </div>
    </footer>
  );
};

export default MoonlogFooter;
