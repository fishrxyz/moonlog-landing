import {
  Cable,
  FolderInput,
  Globe2,
  MailCheck,
  MessageCircle,
  ThumbsUp,
} from "lucide-react";
import features from "../../data/features.json";

const icons = {
  feedback: <MessageCircle size={18} />,
  canny: <FolderInput size={18} />,
  integrations: <Cable size={18} />,
  voting: <ThumbsUp size={18} />,
  domain: <Globe2 size={18} />,
  subs: <MailCheck size={18} />,
};

const FeatureList = () => {
  return (
    <section className="mb-[5em] md:mb-[10em]">
      <header className="text-center md:max-w-3xl mx-auto mb-20 md:px-0">
        <h2 className="font-bold text-3xl mb-4 md:text-5xl">
          Oh, one more thing ...
        </h2>
        <p
          style={{
            opacity: 0.3,
          }}
          className="font-bold text-xl md:text-3xl leading-normal"
        >
          Wait until you see what else we have in the pipe.
        </p>
      </header>

      <div className="max-w-[85%] mx-auto flex flex-col md:max-w-[70%] md:flex-wrap md:justify-between md:flex-row">
        {features.map((feat, idx) => (
          <div key={idx} className="md:basis-[46%] mb-14 md:flex md:flex-row">
            <div className="mb-4 md:mb-0 md:mr-4 table">
              <div className="p-2 bg-slate-100 rounded-full border border-[2px] border-black">
                {icons[feat.icon]}
              </div>
            </div>

            <div>
              <h3 className="font-bold text-xl mb-2">{feat.title}</h3>
              <p className="leading-normal text-slate-500">{feat.text}</p>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
};

export default FeatureList;
