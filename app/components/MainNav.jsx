import Logo from "./Logo";

const MainNav = () => {
  return (
    <div className="flex items-center justify-between">
      <Logo />
      <a
        className="hidden transition-all hover:bg-black hover:text-white px-4 py-3 font-medium bg-slate-100 rounded sm:block"
        href="https://app.moonlog.co/auth/register"
      >
        Try us free
      </a>
    </div>
  );
};

export default MainNav;
