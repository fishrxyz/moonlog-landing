export default function LargeText(props) {
  return (
    <div {...props} className="text-lg font-semibold">
      {props.children}
    </div>
  );
}
